import org.scalatest._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.nio.file.{Files, Path}
import scala.concurrent.Await
import scala.concurrent.duration._

class HumiditySensorStatsSpec extends AnyFlatSpec with Matchers {
  "HumiditySensorStats" should "calculate the correct statistics" in {
    // Create a temporary directory for the test files
    val tempDir = Files.createTempDirectory("humidity-sensor-stats-test")
    // Create the test files
    val file1 = createTestFile(tempDir, "leader-1.csv", Seq(
      "sensor-id,humidity",
      "s1,10",
      "s2,88",
      "s1,NaN",
      ""
    ))
    val file2 = createTestFile(tempDir, "leader-2.csv", Seq(
      "sensor-id,humidity",
      "s2,80",
      "s3,NaN",
      "s2,78",
      "s1,98",
      ""
    ))

    // Run the HumiditySensorStats program with the test files
    val measurementsFuture = HumiditySensorStats.processFiles(tempDir.toString)
    val (processedFiles, processedMeasurements, failedMeasurements, sensorData) =
      Await.result(measurementsFuture, 10.seconds)

    // Check the results
    processedFiles shouldBe 2
    processedMeasurements shouldBe 7
    failedMeasurements shouldBe 2
  }

  def createTestFile(dir: Path, name: String, lines: Seq[String]): Path = {
    val file = dir.resolve(name)
    Files.write(file, lines.mkString("\n").getBytes)
    file
  }
}