import akka.actor.ActorSystem
import akka.stream.scaladsl._
import akka.stream.{IOResult, Materializer}
import akka.util.ByteString
import java.nio.file.{FileSystems, Path}
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.jdk.CollectionConverters._

// Define a case class to hold the sensor data
case class SensorData(sensorId: String, min: Double, avg: Double, max: Double) {
  private val Tolerance = 1e-9

  override def equals(obj: Any): Boolean = obj match {
    case other: SensorData =>
      this.sensorId == other.sensorId &&
        math.abs(this.min - other.min) < Tolerance &&
        math.abs(this.avg - other.avg) < Tolerance &&
        math.abs(this.max - other.max) < Tolerance
    case _ => false
  }
}

object HumiditySensorStats {
  // Create an ActorSystem for Akka Streams
  implicit val system: ActorSystem = ActorSystem("humidity-sensor-stats")
  // Create a Materializer for Akka Streams
  implicit val materializer: Materializer = Materializer(system)
  implicit val ec: ExecutionContextExecutor = system.dispatcher

  def main(args: Array[String]): Unit = {
    // Get the path to the directory containing the CSV files from the command line arguments
    val dirPath = args(0)

    // Run the program with the specified directory path
    val measurementsFuture = processFiles(dirPath)
    measurementsFuture.map { case (processedFiles, processedMeasurements,
    failedMeasurements, sensorData) =>
      // Print the statistics
      println(s"Num of processed files: $processedFiles")
      println(s"Num of processed measurements: $processedMeasurements")
      println(s"Num of failed measurements: $failedMeasurements")
      println("\nSensors with highest avg humidity:\n")
      println("sensor-id,min,avg,max")
      sensorData.foreach { data =>
        println(s"${data.sensorId},${data.min},${data.avg},${data.max}")
      }
    }
  }

  def processFiles(dirPath: String): Future[(Int, Int,
    Int,
    Seq[SensorData])] = {
    // Get a list of files in the directory
    val files = getListOfFiles(dirPath)
    // Process each file and get a Future of the measurements
    val measurementsFutures = files.map(file => processFile(file))
    // Combine the Futures into a single Future of all measurements
    val measurementsFuture = Future.sequence(measurementsFutures).map(_.flatten)
    // When the Future completes, calculate and return the statistics
    measurementsFuture.map { measurements =>
      // Count the number of failed measurements (NaN values)
      val failedMeasurements = measurements.count(_._2.isNaN)
      // Group the measurements by sensor id
      val groupedMeasurements = measurements.groupBy(_._1).view.mapValues(_.map(_._2)).toMap
      // Calculate the min/avg/max for each sensor
      val sensorData = groupedMeasurements.map { case (sensorId, values) =>
        if (values.forall(_.isNaN)) {
          SensorData(sensorId, Double.NaN, Double.NaN, Double.NaN)
        } else {
          val nonNaNValues = values.filterNot(_.isNaN)
          SensorData(sensorId, nonNaNValues.min, nonNaNValues.sum / nonNaNValues.size, nonNaNValues.max)
        }
      }.toSeq.sortBy(data => if (data.avg.isNaN) Double.MaxValue else -data.avg)

      // Return the statistics
      (files.size, measurements.size, failedMeasurements, sensorData)
    }
  }

  // Function to get a list of files in a directory
  def getListOfFiles(dir: String): List[Path] = {
    val directoryStream = FileSystems.getDefault.getPath(dir)
    java.nio.file.Files.newDirectoryStream(directoryStream).iterator().asScala.toList
  }

  // Function to process a file and return a Future of the measurements
  def processFile(file: Path): Future[Seq[(String, Double)]] = {
    // Create a stream from the file
    FileIO.fromPath(file)
      // Split the stream into lines
      .via(Framing.delimiter(ByteString("\n"), maximumFrameLength = Int.MaxValue))
      // Drop the header line
      .drop(1)
      // Split each line into columns
      .map(_.utf8String.split(",").map(_.trim))
      // Map each line to a tuple of sensor id and humidity value
      .map(cols => (cols(0), if (cols(1) == "NaN") Double.NaN else cols(1).toDouble))
      // Collect the results into a sequence
      .runWith(Sink.seq)
  }
}